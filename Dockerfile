FROM golang:1.17.6-buster as build
ENV GOPROXY "https://goproxy.cn"
ENV CGO_ENABLED 0
RUN mkdir /build
COPY . /build/
WORKDIR /build
RUN go build -o servera main.go

FROM golang:alpine3.15
COPY --from=build /build/servera /
COPY --from=build /build/config.yaml /
ENTRYPOINT [ "/servera", "-f","/config.yaml"]
EXPOSE 8888